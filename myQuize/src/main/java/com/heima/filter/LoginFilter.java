package com.heima.filter;

import com.heima.domain.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/*")
public class LoginFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletResponse response = (HttpServletResponse) resp;
        HttpServletRequest request = (HttpServletRequest) req;

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");

        String uri = request.getRequestURI();
        if (uri.contains("login.jsp") || uri.contains("register.jsp") || uri.contains("index.jsp") || uri.contains("/user/login") || uri.contains("/user/register") || uri.contains("/css/") || uri.contains("/js/")) {
            chain.doFilter(req, resp);
            return;
        }

        if (user != null) {
            //已经登陆，放行
            chain.doFilter(req, resp);
        } else {
            //没有登陆，重定向到登陆页面
            System.out.println("没有登陆，转到登陆页面");
            request.getRequestDispatcher("/index.jsp").forward(req, resp);

        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
