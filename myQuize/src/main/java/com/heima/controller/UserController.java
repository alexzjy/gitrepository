package com.heima.controller;

import com.heima.domain.Account;
import com.heima.domain.User;
import com.heima.service.AccountService;
import com.heima.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller("userController")
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/login", produces = "application/json;charset=utf-8")
    @ResponseBody
    public String login(User user, HttpSession session) {
        //用户登陆
        User loginUser = userService.login(user);
        User loginUser2 = userService.login2(user);
//        System.out.println(loginUser);
        if (loginUser2 != null) {
            //登陆成功
            if (loginUser != null) {
                session.setAttribute("user", loginUser);
            } else {
                session.setAttribute("user", loginUser2);
            }
            return "{\"flag\":true}";
        } else {
            return "{\"flag\":false}";
        }
    }

    @RequestMapping(value = "/register", produces = "application/json;charset=utf-8")
    @ResponseBody
    public String register(User user) {
        //用户注册
        boolean flag = userService.register(user);
        if (flag) {
            return "{\"flag\":true}";
        }

        return "{\"flag\":false}";
    }

    @RequestMapping("/logOut")
    public String logOut(HttpSession session) {
//        session.invalidate();
        session.removeAttribute("user");
        return "login";
    }

    @RequestMapping(value = "/delete/{uid}", produces = "application/json;charset=utf-8")
    @ResponseBody
    public String delete(@PathVariable("uid") int uid, HttpSession session) {
        //用户删除
        try {
            User user = (User) session.getAttribute("user");
            List<Account> accountList = user.getAccountList();
            for (Account account : accountList) {
                int aid = account.getAid();
                accountService.delete(aid);
            }
            userService.delete(uid);
            session.invalidate();
            return "{\"flag\":true}";
        } catch (Exception e) {
            return "{\"flag\":false}";
        }
    }
    /*

    @RequestMapping("/update")
    @ResponseBody
    public String update() {
        //用户更新

        return "login";
    }

    @RequestMapping("/register")
    public ModelAndView register(User user) {
        //用户注册
        ModelAndView modelAndView = new ModelAndView();
        userService.register(user);
        modelAndView.setViewName("login");
        return modelAndView;
    }

    @RequestMapping("/findAccounts/{uid}")
    public ModelAndView findAccounts(@PathVariable("uid") int uid,HttpSession session) {
        //查询用户信息
        if(uid<=0){
            User myUser = (User) session.getAttribute("myUser");
            uid=myUser.getUid();
        }
        User userAccounts = userService.findAccounts(uid);
        ModelAndView modelAndView=new ModelAndView();
            modelAndView.addObject("userAccounts",userAccounts);
            session.setAttribute("myUser",userAccounts);
        modelAndView.setViewName("userInfo");
        return modelAndView;
    }

    @RequestMapping("/loginOut")
    public String loginOut(HttpSession session){
        session.invalidate();
        return "login";
    }*/
}
