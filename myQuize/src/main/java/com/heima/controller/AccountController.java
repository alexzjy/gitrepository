package com.heima.controller;

import com.heima.domain.Account;
import com.heima.domain.User;
import com.heima.service.AccountService;
import com.heima.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller("accountController")
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private UserService userService;

    @RequestMapping("/delete/{aid}")

    public String delete(@PathVariable("aid") int aid,HttpSession session){
        accountService.delete(aid);
        User user = (User) session.getAttribute("user");
        List<Account> accountList = user.getAccountList();
        for(int i=0;i<accountList.size();i++){
            Account account = accountList.get(i);
            int aid1 = account.getAid();
            if(aid1==aid){
                accountList.remove(i);
                user.setAccountList(accountList);
                break;
            }
        }
        session.setAttribute("user",user);
        return "userInfo";
    }

    @RequestMapping("/findUser")
    public ModelAndView findUser(){
        List<Account> listOfAccounts= accountService.findAll();
        ModelAndView modelAndView=new ModelAndView();
        modelAndView.addObject("list",listOfAccounts);
        modelAndView.setViewName("accounts");
        return modelAndView;
    }

    @RequestMapping(value = "/create",produces = "application/json;charset=utf-8")
    @ResponseBody
    public String save(int uid,String accountDesc,HttpSession session){
        try {
            Account account = new Account();
            account.setUserId(uid);
            account.setAccountDesc(accountDesc);
            accountService.save(account);
            User user = userService.findAccounts(uid);
            System.out.println(user);
            session.setAttribute("user", user);
            return "{\"flag\":true}";
        }catch (Exception e){
            return "{\"flag\":false}";
        }
    }

}
