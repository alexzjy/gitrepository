package com.heima.service;

import com.heima.domain.Account;

import java.util.List;

public interface AccountService {
    public void save(Account account);

    public void delete(int aid);

    public List<Account> findAll();
}
