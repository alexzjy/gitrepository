package com.heima.service;

import com.heima.domain.User;
import org.springframework.stereotype.Service;

public interface UserService {
    public User login(User user);

    public boolean register(User user);

    public User findAccounts(int uid);

    public void delete(int uid);

    public User login2(User user);
}
