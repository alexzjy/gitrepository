package com.heima.service.impl;

import com.heima.domain.User;
import com.heima.mapper.AccountMapper;
import com.heima.mapper.UserMapper;
import com.heima.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User login(User user) {
        try {
            User loginUser = userMapper.login(user);
            return loginUser;
        } catch (Exception e) {
            return null;
        }

    }

    @Override
    public boolean register(User user) {
        try {
            userMapper.register(user);
        }catch (Exception e){
            return false;
        }
        return true;
    }

    @Override
    public User findAccounts(int uid) {
        return userMapper.findAccounts(uid);
    }

    @Override
    public void delete(int uid) {
        userMapper.delete(uid);
    }

    @Override
    public User login2(User user) {
        return userMapper.login2(user);
    }

}
