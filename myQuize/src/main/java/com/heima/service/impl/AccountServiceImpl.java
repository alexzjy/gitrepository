package com.heima.service.impl;

import com.heima.domain.Account;
import com.heima.mapper.AccountMapper;
import com.heima.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("accountService")
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountMapper accountMapper;

    @Override
    public void save(Account account) {
        accountMapper.save(account);
    }

    @Override
    public void delete(int aid) {
        accountMapper.delete(aid);
    }

    @Override
    public List<Account> findAll() {
        return accountMapper.findAll();
    }
}
