package com.heima.mapper;

import com.heima.domain.Account;

import java.util.List;

public interface AccountMapper {
    public void save(Account account);

    public void delete(int aid);

    public List<Account> findAll();
}
