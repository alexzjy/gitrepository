package com.heima.mapper;

import com.heima.domain.User;

public interface UserMapper {
    public User login(User user);

    public void delete(int uid);

    public void register(User user);

    public User findAccounts(int uid);

    public User login2(User user);
}
