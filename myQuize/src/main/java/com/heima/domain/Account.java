package com.heima.domain;

import java.util.List;

public class Account {
    private int aid;
    private int userId;
    private String accountDesc;
    private User user;

    public Account() {
    }

    public Account(int aid, int userId, String accountDesc, User user) {
        this.aid = aid;
        this.userId = userId;
        this.accountDesc = accountDesc;
        this.user = user;
    }

    public int getAid() {
        return aid;
    }

    public void setAid(int aid) {
        this.aid = aid;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getAccountDesc() {
        return accountDesc;
    }

    public void setAccountDesc(String accountDesc) {
        this.accountDesc = accountDesc;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Account{" +
                "aid=" + aid +
                ", userId=" + userId +
                ", accountDesc='" + accountDesc + '\'' +
                ", user=" + user +
                '}';
    }
}
