package com.heima.domain;

import java.util.List;

public class User {
    private int uid;
    private String username;
    private String password;
    private List<Account> accountList;

    public User() {
    }

    public User(int uid, String username, String password, List<Account> accountList) {
        this.uid = uid;
        this.username = username;
        this.password = password;
        this.accountList = accountList;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Account> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<Account> accountList) {
        this.accountList = accountList;
    }

    @Override
    public String toString() {
        return "User{" +
                "uid=" + uid +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", accountList=" + accountList +
                '}';
    }
}
