<%--
  Created by IntelliJ IDEA.
  User: alex
  Date: 2019-08-02
  Time: 13:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim 和 Respond.js 是为了让 IE8 支持 HTML5 元素和媒体查询（media queries）功能 -->
    <!-- 警告：通过 file:// 协议（就是直接将 html 页面拖拽到浏览器中）访问页面时 Respond.js 不起作用 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/npm/html5shiv@3.7.3/dist/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/respond.js@1.4.2/dest/respond.min.js"></script>
    <![endif]-->
    <style>
        input {
            width: 30%;
        }

        div {
            margin: auto;
            width: 30%;
        }

        button {
            margin: auto;
        }

        form {
            margin: auto;
        }

    </style>
    <script src="../js/jquery-3.3.1.min.js"></script>
</head>
<body>
<h1 align="center">欢迎登陆
    <small><a href="${pageContext.request.contextPath}/index.jsp"> 返回首页</a></small>
</h1>

<form id="loginForm">
    <div class="form-group">
        <label for="exampleInputEmail1">用户名</label>
        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="输入用户名" name="username">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">密码</label>
        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="输入密码" name="password">
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox"> 记住我
        </label>
    </div>
    <div>
        <button id="submit" type="submit" class="btn btn-default">登陆</button>
    </div>
</form>

<div align="center" style="color: red" id="mes"></div>


<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>
</body>

<script>

       $("#loginForm").submit(function () {
           $.post("${pageContext.request.contextPath}/user/login",$(this).serialize(),function (data) {
               if(data.flag){
                   //登陆成功
                   window.location.href="userInfo.jsp";
               }else{
                   alert("用户名或者密码错误");
               }
           });
           return false;
       });

</script>
</html>
