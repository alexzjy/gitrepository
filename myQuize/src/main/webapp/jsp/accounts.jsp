<%--
  Created by IntelliJ IDEA.
  User: alex
  Date: 2019-08-04
  Time: 11:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim 和 Respond.js 是为了让 IE8 支持 HTML5 元素和媒体查询（media queries）功能 -->
    <!-- 警告：通过 file:// 协议（就是直接将 html 页面拖拽到浏览器中）访问页面时 Respond.js 不起作用 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/npm/html5shiv@3.7.3/dist/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/respond.js@1.4.2/dest/respond.min.js"></script>
    <![endif]-->
    <style>
        input {
            width: 30%;
        }

        div {
            margin: auto;
            width: 30%;
        }

        button {
            margin: auto;
        }

        form {
            margin: auto;
            width: 50%;
        }

        .table-bordered {
            margin: auto;
            width: 70%;
        }

    </style>
    <script src="../js/jquery-3.3.1.min.js"></script>
</head>
<body>
<h1 align="center">所有账户信息
    <small><a href="${pageContext.request.contextPath}/index.jsp"> 返回首页</a></small>
</h1>

<table class="table table-bordered">
    <tr>
        <th>账户编号</th>
        <th>所属用户</th>
        <th>账户信息</th>
    </tr>

    <c:forEach items="${list}" var="account">
        <tr>
            <td>${account.aid}</td>
            <td>${account.user.username}</td>
            <td>${account.accountDesc}</td>
        </tr>
    </c:forEach>
</table>


<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>
</body>
</html>
